debug                 = require('debug')('cactuscon-server:server')
RedisPooledJobManager = require 'meshblu-core-redis-pooled-job-manager'
UuidAliasResolver     = require 'meshblu-uuid-alias-resolver'
MessengerFactory      = require './messenger-factory'
RedisNS               = require '@octoblu/redis-ns'
crypto                = require 'crypto'
redis                 = require 'ioredis'
async                 = require 'async'
_                     = require 'lodash'

class Server
  constructor: (options) ->
    {redisUri, namespace, jobTimeoutSeconds, connectionPoolMaxConnections} = options
    {jobLogQueue, jobLogRedisUri, jobLogSampleRate} = options
    {aliasServerUri} = options
    {@meshbluConfig} = options

    @jobManager = new RedisPooledJobManager
      jobLogIndexPrefix: 'metric:cactuscon-server'
      jobLogType: 'cactuscon-server:request'
      jobLogQueue: jobLogQueue
      jobLogRedisUri: jobLogRedisUri
      jobLogSampleRate: jobLogSampleRate
      jobTimeoutSeconds: jobTimeoutSeconds
      maxConnections: connectionPoolMaxConnections
      namespace: namespace
      redisUri: redisUri

    uuidAliasClient = new RedisNS 'uuid-alias', redis.createClient(redisUri)
    uuidAliasResolver = new UuidAliasResolver
      cache: uuidAliasClient
      aliasServerUri: aliasServerUri

    @messengerFactory = new MessengerFactory {uuidAliasResolver, redisUri, namespace}

    @messenger = @buildMessenger()
    @messenger.connect (error) =>
      return @_emitError(error, packet) if error?
      async.each ['received'], (type, next) =>
        @messenger.subscribe {type, uuid: @meshbluConfig.uuid}, next
      , (error) =>
        console.error {error}

    @lastSeen = {}

  _doJob: (jobType, metadata, data, callback) =>
    metadata = _.clone metadata || {}
    metadata.auth = {uuid: @meshbluConfig.uuid, token: @meshbluConfig.token}
    metadata.jobType = jobType
    if data?
      rawData = JSON.stringify data
    job = {metadata, rawData}
    @jobManager.do 'request', 'response', job, (error, response) =>
      debug 'response received:', response
      callback error, response

  searchDevices: (data={}, callback) =>
    @_doJob 'SearchDevices', fromUuid: @meshbluConfig.uuid, data, callback

  update: (uuid, data, callback) =>
    @_doJob 'UpdateDevice', toUuid: uuid, data, callback

  isValidSignature: (message, mac, chipid) =>
    keysha = crypto.createHash('sha1')
    keysha.update 'cactuscon' + mac + chipid
    key = keysha.digest('binary')
    signature_hmac = crypto.createHmac('sha1', key)
    signature_hmac.update message.team
    check_signature = signature_hmac.digest('hex')
    return check_signature == message.signature

  buildMessenger: (replyTopic) =>
    messenger = @messengerFactory.build()
    messenger.on 'message', (channel, message) =>
      debug {message}
      return unless message?.team? and message?.signature?
      return unless message.team == 'white' or message.team=='black'
      lastTime = (Date.now()-@lastSeen[message.fromUuid])/1000
      if lastTime != NaN and lastTime < 60
        console.log 'naughty boy, too soon', message.fromUuid, lastTime
        return
      @lastSeen[message.fromUuid] = Date.now()

      debug 'searching....'
      @searchDevices {uuid:message.fromUuid}, (error, data) =>
        debug {message:data}
        try
          data = JSON.parse(data.rawData)[0]
        catch error
          debug {error}
          return
        debug {data}
        return unless data?.nodemcu
        return unless @isValidSignature(message, data.nodemcu.mac, data.nodemcu.chipid)
        debug 'is valid request'
        @update @meshbluConfig.uuid, {$inc: "#{message.team}Count": 1}, (error, data) =>
          console.log 'updated!', {error, data}

module.exports = Server
